# RSfullstckCG

prérequis :

git 2.21.0

node.js 10.15.3

mongo db 4.0.9

npm 6.4.1

pour lancer le projet entrez les commandes suivantes:

 

-          git clone https://gitlab.com/charlie38110/rsfullstckcg.git

-          cd rsfullstckcg-master

-          npm install

-          npm run development


Ensuite :

Dans un navigateur vous pouvez accédez au site via : http://localhost:3000/
Dès cette instant merci de créer un premier user pour utiliser le RS
Vous pouvez après vous connecter.

Dans l'interface vous avez accès à :

-une page d'acceuil (avec le fil d'actualité des posts de vos amis, la liste des amis possible, une fenètre pour faire vos propre posts)
-une page personnelle (avec votre avatar, vos historique, vos données, vos amis)

sur chaque post vous pouvez liker et commenter.